package com.equintanar.demo.request;

import lombok.Data;

@Data
public class SumRequest {
    private Integer numberA;
    private Integer numberB;
}
