package com.equintanar.demo.controller;

import com.equintanar.demo.request.SumRequest;
import com.equintanar.demo.service.CalculatorService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/calculator")
public class CalculatorController {

    private final CalculatorService calculatorService;

    public CalculatorController(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
    }

    @GetMapping("/sum")
    public ResponseEntity<?> sum(@RequestBody SumRequest request) {
        return ResponseEntity.ok(this.calculatorService.sum(request.getNumberA(), request.getNumberB()));
    }
}
