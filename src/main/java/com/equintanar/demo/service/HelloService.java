package com.equintanar.demo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class HelloService {

    private static final String GREETING = "Hello World, ";

    /**
     * Metodo para saludar
     *
     * @param name el nombre
     */
    public String greeting(String name) {
        log.info("exec greeting: {}", name);
        return GREETING + name;
    }
}
