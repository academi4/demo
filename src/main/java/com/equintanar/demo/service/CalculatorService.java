package com.equintanar.demo.service;

import org.springframework.stereotype.Service;

@Service
public class CalculatorService {
    public int sum(final int a, final int b) {
        return a + b;
    }
}
